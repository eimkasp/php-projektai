<?php

class Projektas {
	public $id;
	public $short_name;
	public $year;
	public $program;
	public $price;

	public static $projects = []; // saugosime visus projektus
	public static $db;

	function __construct($short_name, $year, $program, $price, $id = null) {

		$this->short_name = $short_name;
		$this->year = $year;
		$this->program = $program;
		$this->price = $price;
		$this->id = $id;
	}

	function save() {
		$updateProject = Projektas::$db->conn->prepare("UPDATE projects SET short_name = ?,  year = ?, program = ?, price = ? WHERE id = ?");

    	$updateProject->execute([$this->short_name, $this->year, $this->program, $this->price, $this->id);

    	header('Location: http://localhost/database/index.php');
	}

	static function getById($id) {
		Projektas::$db = new Database();
		$selectProject = "SELECT * FROM `projects` WHERE id = $id";

		$query = Projektas::$db->conn->prepare($selectProject);
		$query->execute();
		$query->setFetchMode(PDO::FETCH_ASSOC);

		// Vieno rezultato gavimas
		$value = $query->fetch();

		$projektas = new Projektas($value['short_name'], $value['year'], $value['program'], $value['price'], $value['id']);

		return $projektas;
	}



	static function getAll() {
		Projektas::$db = new Database();
		$results = Projektas::$db->selectData("SELECT * FROM `projects`");

		foreach ($results as $key => $value) {
			Projektas::$projects[] = new Projektas($value['short_name'], $value['year'], $value['program'], $value['price'], $value['id']);
		}
	}
}