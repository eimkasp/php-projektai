<?php include('header.php'); ?>
<?php

$project_id = isset($_GET['id']) ? $_GET['id'] : 0;

$projektas = Projektas::getById($project_id);

if(isset($_GET['short_name'])) {
    $projektas->short_name = $_GET['short_name'];
    $projektas->year = $_GET['year'];
    $projektas->price = $_GET['price'];
    $projektas->program = $_GET['program'];

    $projektas->save();
    

    $_SESSION['message'] = "Sekmingai paredaguotas projektas";

    header("Location: http://localhost/database");


}



?>
<div class="container">
    <div class="col-md-4">
        <br>
        <br>
<h1>Redaguoti projekta</h1>

<form method="GET" class="form-group">
    <div>
        <input class="form-control mb-2 mt-5" type="text" name="short_name" value="<?php echo $projektas->short_name; ?>">
    </div>

    <div>
        <input class="form-control mb-2" type="number" name="year" value="<?php echo $projektas->year?>">
    </div>

    <div>
        <input class="form-control mb-2" type="text" name="program" value="<?php echo $projektas->program?>">
    </div>

    <div>
        <input class="form-control mb-2" type="number" name="price" value="<?php echo $projektas->price?>">
    </div>

    <input type="hidden" value="<?php echo $projektas->id; ?>" name="id">

    <input  class="btn btn-primary " type="submit" name="">
</form>
    </div>
</div>
</body>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>