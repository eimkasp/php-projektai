<?php include('header.php'); ?>

<?php 

Projektas::getAll();
Projektas::$projects;
?>
<div class="container">
    <div class="col-md-8">
        <br>
        <div>
            <button class="btn btn-primary"><a style="color: white" href="create.php">Sukurti nauja</a></button>
        </div>
        <br>
        <table class="table">
            <thead class="thead-dark">
            <tr class="text-center">
                <th scope="col"><a href="?sort=id&order=<?php echo(($order == 'asc') ? 'desc' : 'asc'); ?>">ID</a></th>
                <th scope="col"><a href="?sort=short_name&order=<?php echo(($order == 'asc') ? 'desc' : 'asc'); ?>">Pavadinimas</a></th>
                <th scope="col"><a href="?sort=year&order=<?php echo(($order == 'asc') ? 'desc' : 'asc'); ?>">Metai</a></th>
                <th scope="col"><a href="?sort=program&order=<?php echo(($order == 'asc') ? 'desc' : 'asc'); ?>">Programa</a></th>
                <th scope="col"><a href="?sort=price&order=<?php echo(($order == 'asc') ? 'desc' : 'asc'); ?>">Suma</a></th>
                <th>Redagavimas</th>
                <th>Istrinti</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach(Projektas::$projects as $projektas) : ?>
                <tr class="text-center">
                     <td><?php echo $projektas->id; ?></td>
                     <td><?php echo $projektas->short_name; ?></td>

                     <td><?php echo $projektas->year; ?></td>

                     <td><?php echo $projektas->program; ?></td>
                     <td><?php echo $projektas->price; ?></td>

                  
                    <td>
                        <a href="edit.php?id=<?php echo $projektas->id; ?>">
                            Redaguoti
                        </a>
                    </td>
                    <td>
                        <a href="delete.php?id=<?php echo $projektas->id; ?>">
                            Delete
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
</body>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>