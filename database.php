<?php
class Database {

	public $servername = "localhost";
	public $username = "root";
	public $password = "root";
	public $dbName = "phpdblesson";
	public $conn;

	function __construct() {
		try {
    		$this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbName", $this->username, $this->password);
		}
		catch(PDOException $e) {
	    	echo "Connection failed: " . $e->getMessage();
		}
	}

	function selectData($select) {
		$query = $this->conn->prepare($select);
		$query->execute();
		$query->setFetchMode(PDO::FETCH_ASSOC);
		return $query -> fetchAll();
	}
}




//// Sukuriame uzklausa
//$query = $conn->prepare("SELECT name, surname, salary FROM darbuotojai");
//// Ivykdome uzklausa
//$query->execute();
//// set the resulting array to associative
//$query->setFetchMode(PDO::FETCH_ASSOC);
//// Gauname visus rezultatus kuriuos grazino uzklausa
//$darbuotojai = $query->fetchAll();

?>

